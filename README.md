# Robots Battle

## Getting Started

Install dependencies:

```
yarn
```

Run dev server:

```
yarn start
```

Go to http://localhost:3000, hask and enjoy! ✨

## Development

To ensure beautiful changelog, please commit with:

```
yarn commit
```

[`git-cz`](https://github.com/streamich/git-cz) inside! 😘

## Deployment

Build first then deploy:

```
$ yarn build
 => Building in /dist
 => ✨  Done in 1.03s.
```
