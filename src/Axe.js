import { random } from "lodash";

import Weapon from "./Weapon";

export default class Axe extends Weapon {
  constructor() {
    super("Axe", random(80, 120), "strikes");
  }
}
