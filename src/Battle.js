import Axe from "./Axe";
import Sword from "./Sword";
import MagicWand from "./MagicWand";
import Robot from "./Robot";

export default class Battle {
  constructor(
    gameScreen,
    fightButton,
    robotAName,
    robotBName,
    robotAHealth,
    robotBHealth
  ) {
    this.gameScreen = gameScreen;
    this.fightButton = fightButton;
    this.robotAName = robotAName;
    this.robotBName = robotBName;
    this.robotAHealth = robotAHealth;
    this.robotBHealth = robotBHealth;

    this.armory = [Axe, Sword, MagicWand];

    this.robotA = new Robot(this.robotAName.value);
    Battle.displayHealth(this.robotA, this.robotAHealth);

    this.robotB = new Robot(this.robotBName.value);
    Battle.displayHealth(this.robotB, this.robotBHealth);
  }

  initGamerScreen() {
    while (this.gameScreen.firstChild) {
      this.gameScreen.firstChild.remove();
    }
  }

  start() {
    this.initGamerScreen();
    // this.fightButton.setAttribute("disabled", true);
    this.fightButton.disabled = true;

    this.displayTextLine(this.robotA.sayHello());
    this.displayTextLine(this.robotB.sayHello());

    this.weaponOfChoice();

    this.timer = setInterval(() => {
      this.displayTextLine(this.robotA.attack(this.robotB));
      if (this.hasWinner()) {
        this.displayWinner();
        return;
      }

      this.displayTextLine(this.robotB.attack(this.robotA));
      if (this.hasWinner()) {
        this.displayWinner();
        return;
      }
    }, 300);
  }

  displayTextLine(text, callback) {
    this.gameScreen.appendChild(document.createTextNode(`${text}\n`));
    Battle.displayHealth(this.robotA, this.robotAHealth);
    Battle.displayHealth(this.robotB, this.robotBHealth);
    if (callback) callback();
  }

  weaponOfChoice() {
    this.displayTextLine(this.robotA.chooseWeapon(this.armory));
    this.displayTextLine(this.robotB.chooseWeapon(this.armory));
  }

  hasWinner() {
    return this.robotA.isDead() || this.robotB.isDead();
  }

  displayWinner() {
    clearInterval(this.timer);
    const winner = [this.robotA, this.robotB].find((robot) => !robot.isDead());

    this.displayTextLine(`The winner is ${winner.name}!`, () => {
      this.fightButton.disabled = false;
    });
  }

  static displayHealth(robot, robotHealth) {
    robotHealth.firstChild.remove();
    const robotAHealthTextNode = document.createTextNode(`${robot.health} ❤️`);
    robotHealth.appendChild(robotAHealthTextNode);
  }
}
