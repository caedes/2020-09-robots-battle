import { random } from "lodash";

import Weapon from "./Weapon";

export default class MagicWand extends Weapon {
  constructor() {
    super("MagicWand", random(30, 150), "casts a spell on");
  }
}
