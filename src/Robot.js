import { random } from "lodash";

export default class Robot {
  constructor(name) {
    this.name = name;
    this.health = 1000;
    this.weapon = null;
  }

  sayHello() {
    return `Hello, I'm ${this.name}`;
  }

  chooseWeapon(armory) {
    this.weapon = new armory[random(armory.length - 1)]();
    return `${this.name} choose ${this.weapon.name}`;
  }

  attack(opponent) {
    opponent.health = Math.max(opponent.health - this.weapon.damage, 0);
    return `${this.name} ${this.weapon.verb} ${opponent.name}`;
  }

  isDead() {
    return this.health <= 0;
  }
}
