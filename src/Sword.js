import { random } from "lodash";

import Weapon from "./Weapon";

export default class Sword extends Weapon {
  constructor() {
    super("Sword", random(90, 100), "slashes");
  }
}
