export default class Weapon {
  constructor(name, damage, verb = "hits") {
    this.name = name;
    this.damage = damage;
    this.verb = verb;
  }
}
