import Battle from "./Battle";

const robotAName = document.getElementById("robotAName");
const robotBName = document.getElementById("robotBName");
const robotAHealth = document.getElementById("robotAHealth");
const robotBHealth = document.getElementById("robotBHealth");

const gameScreen = document.getElementById("game-screen");

const fightButton = document.getElementById("fight");
fightButton.addEventListener("click", () => {
  const battle = new Battle(
    gameScreen,
    fightButton,
    robotAName,
    robotBName,
    robotAHealth,
    robotBHealth
  );
  battle.start();
});
